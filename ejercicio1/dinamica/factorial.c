int factorial(int number){
    long int result=0;
    long int fact = number;
    result = 1;
    while(fact > 1) {
       result *= fact;
       fact--;
    }
    return result; 
}
